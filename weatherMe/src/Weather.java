
import java.math.BigDecimal;
import java.util.Date;


public class Weather {
    long id;
    String city;
    BigDecimal temperature;
    Date readingDate;
    
    @Override
    public String toString() {
        return String.format("%d: in %s was %sC on %s", id, city, temperature, readingDate);
    }
}
