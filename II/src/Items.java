
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class InvalidInputDataException extends Exception {

    InvalidInputDataException(String message) {
        super(message);
    }
}

abstract class Item implements Comparable<Item> {

    String name;
    double weight;

    public Item(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public int compareTo(Item o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return String.format("15%s  %5.2f", name, weight);
    }

}

class TV extends Item {

    double diagonal;

    public TV(String name, double weight, double diagonal) {
        super(name, weight);
        this.diagonal = diagonal;
    }

    @Override
    public String toString() {
        return String.format("%20s  %5.2f kg  %5.2f inch", name, weight, diagonal);
    }
}

class Couch extends Item {

    int seat;

    public Couch(String name, double weight, int seat) {
        super(name, weight);
        this.seat = seat;
    }

    @Override
    public String toString() {
        return String.format("%20s  %5.2f kg  %4d seats", name, weight, seat);
    }
}

class Bed extends Item {

    double width, height;

    public Bed(String name, double weight, double width, double height) {
        super(name, weight);
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return String.format("%20s  %5.2f kg   %5.2f inch  %5.2f inch", name, weight, width, height);
    }
}

class sortWeight implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        if (o1.weight == o2.weight) {
            return 0;
        } else if (o1.weight > o2.weight) {
            return 1;
        }
        return -1;
    }
}

class sortNameWeight implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        if (o1.name == o2.name) {
            if (o1.weight == o2.weight) {
                return 0;
            } else if (o1.weight > o2.weight) {
                return 1;
            }
            return -1;
        } else {
            return o1.name.compareTo(o2.name);
        }
    }
}

public class Items {

    static ArrayList<Item> itemList = new ArrayList<>();
    static final String filename = "input.txt";

    public static void main(String[] args)
            throws InvalidInputDataException {

        String name = "";
        double weight, diagonal, length, width, height;
        int seat;
        try {
            Scanner fileInput = new Scanner(new File(filename));
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String[] data = line.split(";");
                try {
                    if (data[0].equals("TV")) {
                        if (data.length != 4) {
                            throw new InvalidInputDataException("skip the line");
                        }
                        name = data[1];
                        weight = Double.parseDouble(data[2]);
                        diagonal = Double.parseDouble(data[3]);
                        itemList.add(new TV(name, weight, diagonal));
                    }

                    if (data[0].equals("Couch")) {
                        if (data.length != 4) {
                            throw new InvalidInputDataException("skip the line");
                        }
                        name = data[1];
                        weight = Double.parseDouble(data[2]);
                        seat = Integer.parseInt(data[3]);
                        itemList.add(new Couch(name, weight, seat));
                    }

else {
                        throw new InvalidInputDataException("skip the line");
                    }
                } catch (InvalidInputDataException ex) {
                }
            }

            System.out.println("Normal order: ");
            for (Item o : itemList) {
                System.out.println(o.toString());
            }

            Collections.sort(itemList);
            System.out.println("\n Order by  name: ");
            for (Item o : itemList) {
                System.out.println(o.toString());
            }

            Collections.sort(itemList, new sortWeight());
            System.out.println("\n Order by weight: ");
            for (Item o : itemList) {
                System.out.println(o.toString());
            }

            Collections.sort(itemList, new sortNameWeight());
            System.out.println("\n Order by name and weight: ");
            for (Item o : itemList) {
                System.out.println(o.toString());
            }

        } catch (FileNotFoundException ex) {

        }
    }

}
