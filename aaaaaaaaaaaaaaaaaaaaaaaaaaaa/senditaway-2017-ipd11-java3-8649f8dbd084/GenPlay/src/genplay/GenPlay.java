package genplay;

import java.util.ArrayList;
import java.util.Scanner;

// Generics give us TYPE SAFETY at compilation time
class Box<T> {

    private T itemInside;

    T getContents() {
        return itemInside;
    }

    void putContents(T item) {
        itemInside = item;
    }
}

public class GenPlay {

    public static void main(String[] args) {
        
        Box<String> boxOfString = new Box<>();        
        String contents = boxOfString.getContents();
        
        Box<Integer> boxOfInt = new Box<>();
        boxOfInt.putContents(new Integer(6));
        
        
        ArrayList<String> oldList = new ArrayList<>();

        oldList.add("Jerry");
//        oldList.add(new Scanner("aaaa"));
//        oldList.add(new Object());
        oldList.add("Marry");

        for (int i = 0; i < oldList.size(); i++) {
            String s = (String) oldList.get(i);
            System.out.println(s);
        }

    }

}
