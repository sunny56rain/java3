HOMEWORK

PART 1
======

To MyFriends project add two methods

public void saveListToFile() {}
public void loadListFromFile() {}

Call loadListFromFile() in construtor to load list of friends from friends.txt file.
One name per line.
Make sure you handle IOExceptions by printing stack trace and showing dialog to the user.
You must use try-with-resources for accessing files.

Intercept application's quit event and call saveListToFile() there.


PART 2
======

Implement Ice Cream selector (ScoopSelector projecT) as in the photo posted on the forum.


PART 3
======

Finish the Quiz if you haven't done so yet.
