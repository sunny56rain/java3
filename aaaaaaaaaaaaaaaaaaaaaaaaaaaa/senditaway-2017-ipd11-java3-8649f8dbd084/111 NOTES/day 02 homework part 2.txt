HOMEWORK PART 1

Create a new project JNotepad that will be a close clone of Notepad in MS Windows.

You must use Border Layout as we discussed.

1. Add Filters for (Text files) *.txt and (All files) *.* to file Open / Save dialogs.

You must also make sure that the proper extension is used (.txt) when saving a file.


File menu options:
- Open ...
- Save
- Save as ...
- Close
- Exit


2. Add implement File->Close and File->Save
options.

File->Close will clean the TextArea and set currentFile to null.


3. Do not allow user to loose unsaved text.
- any unsaved changes when no file was open
- any unsaved changes on current file

That includes handling of:
* File->Exit
* Closing window by clicking X on the frame

Use JPaneDialog with Yes/No/Cancel choices.

Find an event handler that will be called
when textArea is modifed.

Also, display "(modified)" in the title of your program when text was modified but not saved yet.

https://stackoverflow.com/questions/7740465/text-changed-event-in-jtextarea-how-to

4. Implement Settings->Font color menu item
that uses Color chooser to change color of text in TextArea.

5. Implement Settings->Word Wrap menu item as a check-box menu item that decides whether word wrap is enabled in TextArea.

6. Implement accellerators for the menu.
Ctrl+S is save, Ctrl-Q is exit, Ctrl-O is open.

