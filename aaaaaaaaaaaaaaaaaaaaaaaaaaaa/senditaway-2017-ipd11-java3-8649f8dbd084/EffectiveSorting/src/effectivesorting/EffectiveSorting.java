package effectivesorting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

class Person implements Comparable<Person> {

    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Person o) {
        return age - o.age;
    }
    
    @Override
    public String toString() {
        return name + "-" + age;
    }
}

public class EffectiveSorting {

    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<>();
        people.add(new Person("Jerry", 33));
        people.add(new Person("Terry", 12));
        people.add(new Person("Barry", 44));
        people.add(new Person("Olary", 31));
        System.out.print("Orignal order: ");
        for (Person p: people) {
            System.out.print(p + ",");
        }
        System.out.println("");
        Collections.sort(people);
        System.out.print("By age order: ");
        for (Person p: people) {
            System.out.print(p + ",");
        }
        System.out.println("");
        
    }

}
