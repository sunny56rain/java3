package enumperson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

enum Months { }
enum DaysOfWeek { }

class Person {
	String name;
	Gender gender;
        //char gender; // 'm' 'f' '-'
        
	AgeRange ageRange;        
	
	enum Gender { Male, Female, NA }
	enum AgeRange { Below18, From18to35, From35to65, Over65 }	
	
	@Override
	public String toString() {
            return String.format("%s;%s;%s", name, gender, ageRange);
        }
}

public class EnumPerson {

    public static void main(String[] args) throws IOException {
        
        /*
        Person p1 = new Person();
        p1.name = "jerry";
        p1.gender = Person.Gender.NA;
        p1.ageRange = Person.AgeRange.From35to65;
        
        p1.gender = Person.Gender.valueOf("Male");
        
        System.out.println("p1: " + p1);
        
        System.out.println("p1.gender as int: " + p1.gender.ordinal());
        */
        
        ArrayList<Person> list = new ArrayList<>();
        Scanner in = new Scanner(new File("input.txt"));
        while (in.hasNextLine()) {
            String line = in.nextLine();
            String data[] = line.split(";");
            Person p = new Person();
            p.name = data[0];
            p.gender = Person.Gender.valueOf(data[1]);
            p.ageRange = Person.AgeRange.valueOf(data[2]);
            list.add(p);
        }
        for (Person p : list) {
            System.out.println(p);
        }
    }
    
}
