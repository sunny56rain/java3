package bestcar;

import java.util.Comparator;

class Car implements Comparable<Car> {

    private static int counter;
    int uniqueId;
    
    public static int getCount() { return counter; }
    public int getUnique() { return uniqueId; }
    
    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKmph; // maximum speed in km/h
    double secTo100Kmph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km

    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        counter++;
        uniqueId = counter;
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
    }

    @Override
    public int compareTo(Car o) {
        // compare in case-insensitive manner
        int result = make.toLowerCase().compareTo(o.make.toLowerCase());
        if (result != 0) {
            return result;
        }
        // compare in case-insensitive manner
        return model.toLowerCase().compareTo(o.model.toLowerCase());
    }

    // comparator as an anonymous class
    static Comparator<Car> comparatorByMaxSpeed = new Comparator<Car>() {

        @Override
        public int compare(Car o1, Car o2) {
            return o1.maxSpeedKmph - o2.maxSpeedKmph;
        }
    };
}
// Usage: Collections.sort(list, Car.comparatorByMaxSpeed);
// Usage (if no static field): Collections.sort(list, new CarComparattorByMaxSpeed());

