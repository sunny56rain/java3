package bestcar;

import java.util.ArrayList;

public class Stack<T> {

    private ArrayList<T> storage = new ArrayList<>();

    public int getHeight() {
        return storage.size();
    }

    public void push(T item) {
        storage.add(0, item);
    }

    public T pop() {
        return storage.remove(0);
    }
}
