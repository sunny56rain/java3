
package bestcar;

import java.util.ArrayList;

public class BestCar {

    static ArrayList<Car> garage = new ArrayList<>();

    public static void main(String[] args) {
        
        // Car.counter;
        
        System.out.println("Initial: " + Car.getCount());
        new Car("Audi", "A5", 1, 2, 2);
        new Car("Audi", "A5", 1, 2, 2);
        new Car("Audi", "A5", 1, 2, 2);
        System.out.println("After: " + Car.getCount());
        
    }
    
}
