
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Database {
    public static final String DBUSER = "flavors";
    public static final String DBPASS = "eSqgvSOsdemw2Oor";
    
    private Connection conn;
    
    public Database() throws SQLException {
        //eSqgvSOsdemw2Oor
        
                try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/flavors", DBUSER, DBPASS);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
    }
    
     public ArrayList<Flavor> getAllFlavors() throws SQLException {
        final String query = "SELECT * FROM flavors";
        ArrayList<Flavor> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int id = rs.getInt("ID");
                String name = rs.getString("name");
                Flavor f = new Flavor(id, name);
                result.add(f);
          //      System.out.println(f);
            }
        }
        return result;
    }
     public void placeorder(Order po) throws SQLException {
        String query = "INSERT INTO placeorder VALUES (NULL, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, po.customerName);
       
        ps.setString(2, po.flavorList);
        ps.execute();
    }
    
 public ArrayList<Order> getAllOrders() throws SQLException {
        final String SELECT_ALL_REPS = "SELECT * FROM placeorder";
        ArrayList<Order> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ALL_REPS);
            while (rs.next()) {
                int id = rs.getInt("placeorderID");
                String name = rs.getString("CustomerName");
                String flavorLast = rs.getString("flavorList");
                Order o = new Order(id, name, flavorLast);
                result.add(o);
            }
        }
        return result;
    }
     
}