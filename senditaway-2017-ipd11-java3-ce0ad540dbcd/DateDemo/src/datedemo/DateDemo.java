/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datedemo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author teacher
 */
public class DateDemo extends javax.swing.JFrame {

    Random random = new Random();
    
    /**
     * Creates new form DateDemo
     */
    public DateDemo() {
        initComponents();
        
        boolean isChecked = random.nextBoolean();
        cbPastAnniversary.setSelected(isChecked);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        tfDateFirst = new javax.swing.JTextField();
        btParseDate = new javax.swing.JButton();
        cbPastAnniversary = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        tfDateSecond = new javax.swing.JTextField();
        btCompareDates = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Enter date DD/MM/YYYY:");

        btParseDate.setText("Parse date");
        btParseDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btParseDateActionPerformed(evt);
            }
        });

        cbPastAnniversary.setText("Past anniversary");

        jLabel2.setText("Enter date DD/MM/YYYY:");

        btCompareDates.setText("Compare dates");
        btCompareDates.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCompareDatesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfDateFirst, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(cbPastAnniversary, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btParseDate))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfDateSecond, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(162, 162, 162)
                        .addComponent(btCompareDates)))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfDateFirst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btParseDate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfDateSecond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btCompareDates)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 103, Short.MAX_VALUE)
                .addComponent(cbPastAnniversary)
                .addGap(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static SimpleDateFormat defaultDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    public static SimpleDateFormat forFileDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    private void btParseDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btParseDateActionPerformed
        try {
            boolean isPast = cbPastAnniversary.isSelected();
            System.out.println("Is past: " + isPast);
            
            String strDate = tfDateFirst.getText();
            Date date = defaultDateFormat.parse(strDate);
            System.out.println("toString() result from Date was: " + date.toString());
            System.out.println("custom format     from Date was: " + forFileDateFormat.format(date));
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "Input error",
                        JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btParseDateActionPerformed

    private void btCompareDatesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCompareDatesActionPerformed
        try {
            Date first = defaultDateFormat.parse(tfDateFirst.getText());
            Date second = defaultDateFormat.parse(tfDateSecond.getText());
            int result = first.compareTo(second);
            if (result == 0) {
                System.out.println("dates are identical");
            } else if (result > 0) {
                System.out.println("first date is later");
            } else {
                System.out.println("second date is later");
            }
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "Input error",
                        JOptionPane.ERROR_MESSAGE);
        }
        
        
    }//GEN-LAST:event_btCompareDatesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DateDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DateDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DateDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DateDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DateDemo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCompareDates;
    private javax.swing.JButton btParseDate;
    private javax.swing.JCheckBox cbPastAnniversary;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField tfDateFirst;
    private javax.swing.JTextField tfDateSecond;
    // End of variables declaration//GEN-END:variables
}
