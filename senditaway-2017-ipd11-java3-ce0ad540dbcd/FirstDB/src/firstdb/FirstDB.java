package firstdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class FirstDB {

    public static final String DBUSER = "firstdb";
    public static final String DBPASS = "8oMkmetNrCOx5Ih7";
    
    public static void main(String[] args) throws SQLException {
        Scanner input = new Scanner(System.in);
        // connect to the database        
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/firstdb", DBUSER, DBPASS);
        
        // ask for data to insert
        System.out.print("Enter name of city: ");
        String city = input.nextLine();
        System.out.print("Enter temperature: ");
        String temp = input.nextLine();
        System.out.print("Enter date in YYYY-MM-DD format: ");
        String date = input.nextLine();
        
        // do the insert
        String sql = "INSERT INTO weather (city, temperature, readingDate) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, city);
            stmt.setString(2, temp);
            stmt.setString(3, date);
            stmt.executeUpdate();
        }
        
    }
    
}
