/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aaaaa;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class Trip {
    private String Dest;
    private String Name;
    private String Passport;
    private Date Depa;
    private Date Return;

    public Trip(String Dest, String Name, String Passport, Date Depa, Date Return) {
        setDest(Dest);
        setName(Name);
        setPassport(Passport);
        setDepa(Depa);
        setReturn(Return);
    }

    
    
    public String getDest() {
        return Dest;
    }

    public void setDest(String Dest) {
        this.Dest = Dest;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPassport() {
        return Passport;
    }

    public void setPassport(String Passport) {
        this.Passport = Passport;
    }

    public Date getDepa() {
        return Depa;
    }

    public void setDepa(Date Depa) {
        this.Depa = Depa;
    }

    public Date getReturn() {
        return Return;
    }

    public void setReturn(Date Return) {
        this.Return = Return;
    }
    
    @Override
    public String toString() {
        return String.format("%s(%s) to %s from %tF to %tF", Dest, Name, Passport, Depa, Return);
    }
    
    
}
