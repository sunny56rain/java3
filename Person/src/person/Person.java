
package person;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import javax.swing.JOptionPane;


class InvalidInputDataException extends Exception {

    InvalidInputDataException(String message) {
        super(message);
    }
}

abstract class Person implements Comparable<Person>{

    String name;
    int age;

   public Person(String n, int a) {
       
        this.name=name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 2  ) {
            throw new IllegalArgumentException("name must be 2 or more characters");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("age must be in 0-150");
        }
        this.age = age;
    }
    
        @Override
    public String toString() {
        return String.format("%s  %d", name, age);
    }

    
    @Override
    public int compareTo(Person o) {
        return this.name.compareTo(o.name);
    }
    

}

class Student extends Person {

    private String program;
    private double gpa;

    public Student(String name, int age, String prog, double gpa) {
        super(name, age);
        this.program = prog;
        this.gpa = gpa;
    }
    @Override
    public String toString() {
        return String.format("%s  %d  %s %d", name, age, program, gpa);
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
         if (program.length() < 2  ) {
            throw new IllegalArgumentException("program must be 2 or more characters");
        }
        this.program = program;
    }
    

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        if (gpa < 1 || gpa > 4.0) {
            throw new IllegalArgumentException("gpa muts in 1-4.0");
        }
        this.gpa = gpa;
    }
}

class Teacher extends Person {

    private String subject;
    private int yoe;

    Teacher(String n, int a, String subj, int yo) {
        super(n, a);
        this.subject = subj;
        yoe = yo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        if (subject.length() < 2  ) {
            throw new IllegalArgumentException("subject must be 2 or more characters");
        }
        this.subject = subject;
    }

    public int getYoe() {
        return yoe;
    }

    public void setYoe(int yoe) {
        this.yoe = yoe;
    }
    @Override
    public String toString() {
        return String.format("%s  %d  %s %d", name, age, subject, yoe);
    }
}

class sortAge implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
       if (o1.age == o2.age) {
            return 0;
        } else if (o1.age > o2.age) {
            return 1;
        }
        return -1; 
    }
}


class Person {

    static ArrayList<Person> itemList = new ArrayList<>();
    static final String filename = "input.txt";

    public static void main(String[] args)
            throws InvalidInputDataException {

        String name = "";
        String subject = "";
        String program = "";
        int age = 0;
        int yoe;
        double gpa;
        
        try {
            Scanner fileInput = new Scanner(new File(filename));
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String[] data = line.split(";");
                try {
                    if (data[0].equals("Student")) {
                        if (data.length != 5) {
                            throw new InvalidInputDataException("skip the line");
                        }
                        name = data[1];
                        age = Integer.parseInt(data[2]);
                        program = data[3];
                        gpa = Double.parseDouble(data[4]);
                        itemList.add(new Student(name, age, program, gpa));
                    }

                    if (data[0].equals("Teacher")) {
                        if (data.length != 5) {
                            throw new InvalidInputDataException("skip the line");
                        }
                        name = data[1];
                        age = Integer.parseInt(data[2]);
                        subject = data[3];
                        yoe = Integer.parseInt(data[4]);
                        itemList.add(new Teacher(name, age, subject, yoe));
                    } else {
                        throw new InvalidInputDataException("skip the line");
                    }
                } catch (InvalidInputDataException ex) {
                }
            }

            System.out.println("Normal order: ");
            for (Item o : itemList) {
                System.out.println(o.toString());
            }

            Collections.sort(itemList);
            System.out.println("\n Order by  name: ");
            for (Item o : itemList) {
                System.out.println(o.toString());
            }

            Collections.sort(itemList, new sortWeight());
            System.out.println("\n Order by age: ");
            for (Item o : itemList) {
                System.out.println(o.toString());
            }
        } catch (FileNotFoundException ex) {

        }
    }
}






 
