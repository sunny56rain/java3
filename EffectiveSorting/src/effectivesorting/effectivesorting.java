/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;

 class Person implements Comparable<Person> {
	private String name;
	private int age;
        
	Person (String name, int age) {
		this.name = name;
		this.age = age;
	}
        
@Override
public int compareTo (Person o) {
return age - o.age;
}

@Override
public String toString()  {
return name + "-" +age;
}
 }

public class effectivesorting{

	public static void main(String args[]){

            ArrayList<Person> people = new ArrayList<>();
            
            people.add(new Person("Peter",17));
            people.add(new Person("Mary", 18));
            people.add(new Person("John", 19));
            people.add(new Person("Ken", 20));
            people.add(new Person("Jerry", 16));
            people.add(new Person("Gull", 15));
            people.add(new Person("Judy", 14));
            System.out.print("Orignal order: ");
            for (Person p: people){
                System.out.print(p + ",");
            }
            System.out.println("");
            Collections.sort(people);
            System.out.println("By age order: ");
           for (Person p: people) {
                System.out.print(p + ",");
            }
            System.out.println("");

        }
}


    
    

