/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author ipd11
 */
public class GenPlay {

    public class Stack<T> {
	public Stack() { }
	public int getHeight() { 
            return 0;
    }
	public void push(T item) { ... }
	public T pop() { return null;
    }
}
    public static void main(String[] args) {
	Stack<Car> stackOfCars = new Stack<>();
	Stack<String> stackOfStrings = new Stack<>();
	
	System.out.printf("Stack of cars is %d tall, strings is %d tall", 
		stackOfCars.getHeight(), stackOfStrings.getHeight());
		
	stackOfCars.push(new Car());
	stackOfCars.push(new Car());

	System.out.printf("Stack of cars is %d tall, strings is %d tall", 
	stackOfCars.getHeight(), stackOfStrings.getHeight());

	Car c2 = stackOfCars.pop();
	Car c1 = stackOfCars.pop();
	Car c0 = stackOfCars.pop(); // should return null, no more cars left
	
    }
    
}
