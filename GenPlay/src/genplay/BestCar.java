/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestcar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Car implements Comparable<Car>{
	private String make; // e.g. BMW, Toyota, etc.
	private String model; // e.g. X5, Corolla, etc.
	private int maxSpeedKmph; // maximum speed in km/h
	private double secTo100Kmph; // accelleration time 1-100 km/h in seconds
	private double litersPer100km; // economy: liters of gas per 100 km
        private static int counter;
        int uniqueId;
        
    public static int getCount() { return counter; }
    
public int getUniqueId() {return uniqueId;}
    
       
        
    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        uniqueId = counter;
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
        counter++;
       
    }
    
    @Override
    public int compareTo(Car o) {
    int result = make.toLowerCase().compareTo(o.make.toLowerCase());
    if (result != 0) {
        return result;
    }    
         return model.toLowerCase().compareTo(o.model.toLowerCase());
    
    }
   static Comparator<Car> comparatorByMaxSpeed = new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.maxSpeedKmph - o2.maxSpeedKmph;
            }
    };
   
   static Comparator<Car> comparatorByAccelleration = new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
               if (o1.secTo100Kmph == o2.secTo100Kmph) return 0;
		
		if (o1.secTo100Kmph > o2.secTo100Kmph) return 1;  
                else return -1;               
            }
            
    };

   static Comparator<Car> comparatorByEconomy = new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
            if (o1.litersPer100km == o2.litersPer100km) return 0;
		
		if (o1.litersPer100km > o2.litersPer100km) return 1;  
                else return -1;       
                
            }
            
    };

//Usage: Collections.sort(list, Car.comparatorByMaxSpeed);
//Usage (if no static field): Collections.sort(list, new CarcomparatorByMaxSpeed());
   
   
  
    
    @Override
    public String toString()  {
        return uniqueId + " "  + make + " " + model + " " + maxSpeedKmph + " " + secTo100Kmph + " " + litersPer100km;
    }

 


    
    

}

public class BestCar {

 
    public static void main(String[] args) {
        
         ArrayList<Car> garage = new ArrayList<>();
            
            garage.add(new Car("BMW","X5",280,7,10));
            garage.add(new Car("BMW","X1",280,9,11));
            garage.add(new Car("Toyota","RAV4",260,9,9));
            garage.add(new Car("Toyota","Corolla",220,10,7));
            garage.add(new Car("Toyota","Echo",200,10,6));
            garage.add(new Car("Toyota","Matrix",200,10,6));
            garage.add(new Car("BENZ","GL450",280,7,10));
            garage.add(new Car("BENZ","GL350",280,7,11));
            garage.add(new Car("BENZ","GL63",300,7,15));
            garage.add(new Car("BMW","X6",280,6,13));
            
            System.out.println("Orignal order: ");
            for (Car c: garage){
                System.out.println(c + ",");
            }
            System.out.println("");
            
            Collections.sort(garage);
            System.out.println("By make and model order: ");
            for (Car c: garage) {
                System.out.println(c + ",");
            }
            System.out.println("");
            
            Collections.sort(garage, Car.comparatorByMaxSpeed);
            System.out.println("By MaxSpeed order: ");
            for (Car c: garage) {
                System.out.println(c + ",");
            }
            System.out.println("");
            
            Collections.sort(garage, Car.comparatorByAccelleration);
            System.out.println("By Accelleration order: ");
            for (Car c: garage) {
                System.out.println(c + ",");
            }
            System.out.println("");
            
            Collections.sort(garage, Car.comparatorByEconomy);
            System.out.println("By Economy order: ");
            for (Car c: garage) {
                System.out.println(c + ",");
            }
            System.out.println("");

        System.out.println(Car.getCount());
        

}   
}

