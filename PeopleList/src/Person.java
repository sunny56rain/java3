/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipd11
 *
 */
public class Person {

    public Person(String name, int age, String postalcode) {
        setName(name);
        setAge(age);
        setPostalcode(postalcode);
    }

    private String name;
    private int age;
    private String postalcode;

    @Override
    public String toString() {
        return String.format("%s is %d y/o, Postal Code is %s", name, age, postalcode);
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        if (name.length() < 1 || name.length() > 50 || name.contains(";")) {
            throw new IllegalArgumentException("Name must be between 1 and 50 characters long");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public final void setAge(int age) {
        if (age < 1 || age > 150) {
            throw new IllegalArgumentException("Age must be between 1 and 150 ");
        }
        this.age = age;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        if (!postalcode.matches("^[A-Z][0-9][A-Z] [0-9][A-Z][0-9]$")) {
            throw new IllegalArgumentException("Postal Code must be like H9H 0A0 ");
        }
        this.postalcode = postalcode;
    }
}
