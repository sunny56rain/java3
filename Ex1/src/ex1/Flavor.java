/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author Administrator
 */
public class Flavor {
    int id;
    String name;

    public Flavor(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
                   if (name == null || name.length() < 2) {
            throw new IllegalArgumentException("name was too short");
        }
        this.name = name;
    }
    
    @Override
    public String toString() {
        return id + "  " + name;
    }
}
