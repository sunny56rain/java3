/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author Administrator
 */
public class Order {
    int id;
    String customerName;
    String flavorList;

    public Order(int id, String customerName, String flavorList) {
        this.id = id;
        this.customerName = customerName;
        this.flavorList = flavorList;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
                     if (customerName==null||customerName.length()<2){
            throw new IllegalArgumentException("name too short");
        }
        this.customerName = customerName;
    }

    public String getFlavorList() {
        return flavorList;
    }

    public void setFlavorList(String flavorList) {
                   if (flavorList.length()==0){
            throw new IllegalArgumentException("Flavor too short");
        }
        this.flavorList = flavorList;
    }
    
    
         @Override
    public String toString() {
        return  id + "   " + customerName + " ordered " + flavorList ;
    }
}
