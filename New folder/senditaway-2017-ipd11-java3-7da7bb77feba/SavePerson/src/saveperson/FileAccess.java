/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveperson;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author teacher
 */

// This class is Model layer 
public class FileAccess {
    
    public static final String FILE_NAME = "people.txt";
    
    public static void addPersonToFile(String line) throws IOException {
            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME, true)))) {
                out.println(line);                
            }
    }
}
