* Exception handling
- try, catch, finally, throw, throws
- The difference between check and unchecked exceptions

* Text file I/O
- reading and writing text files
- handling exceptions

* Parsing Integers and Doubles

* Parsing input from text file using Scanner

* Using String.format() and System.out.printf() methods to format integers, strings, and floating point values.