/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicrm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import minicrm.entity.Customer;
import minicrm.entity.Representative;

/**
 *
 * @author ipd
 */
public class Database {
     
        private Connection conn;
    public Database() throws SQLException {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:sqlite:minicrm.db");
        } catch (SQLException e) {
        }

    }
    
     public ArrayList<Customer> getAllCustomers() throws SQLException {
       
      final String SELECT_PERSON = "SELECT * from customer";
      ArrayList<Customer> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_PERSON);
            while (rs.next()) {
                int CustomerID = rs.getInt("CustomerID");
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                String address = rs.getString("address");
                  String postalCode = rs.getString("postalCode");
                  String phoneNumber = rs.getString("phoneNumber");
                  String SIN = rs.getString("SIN");
                Customer c = new Customer(CustomerID, nameFirst,nameLast,address,postalCode,phoneNumber,SIN);
//                System.out.println(c.CustomerID);
                result.add(c);
            }
        }
        return result;
    }
        
        public void addCustomer(int CustomerID, String nameFirst, String nameLast, String address, String postalCode, String phoneNumber, String SIN) throws SQLException {
        String query = "Insert into customer values (null, ?,?,?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, nameFirst);
        ps.setString(2, nameLast);
        ps.setString(3, address);
        ps.setString(4, postalCode);
        ps.setString(5, phoneNumber);
        ps.setString(6, SIN);
        ps.execute();
    }
     
         public ArrayList<Representative> getAllRepresentative() throws SQLException {
       
      final String SELECT_Representative = "SELECT * from Representative";
      ArrayList<Representative> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_Representative);
            while (rs.next()) {
                int representativeID = rs.getInt("repID");
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                
                Representative r = new Representative( representativeID, nameFirst,nameLast);
                result.add(r);
            }
        }
        return result;
    }
         
        public void addRepresentative(String nameFirst, String nameLast) throws SQLException {
        String query = "Insert into person values (null, ?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, nameFirst);
        ps.setString(2, nameLast);        
        ps.execute();
    }
    
}
