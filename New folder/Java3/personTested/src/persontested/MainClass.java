/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persontested;

import java.util.ArrayList;

/**
 *
 * @author ipd
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            people.add(new Person("Jerry", 33));
            people.add(new Person("Eva", 22));
            people.add(new Person("Jason", 44));
            people.add(new Person("mary", 55));
            
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        for (Person p: people){
            System.out.println(p);
        }
        
        
    }
    
}
