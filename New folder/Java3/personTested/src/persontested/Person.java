/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persontested;

import java.util.Date;

/**
 *
 * @author ipd
 */
public class Person {
    
   private String name;
   private int age;
   // homework 1)encapsulate field 2) write setter 3) writer unit test for each setter
   String postalCode; // "7A7 A7A"
   double weightKg; //0-300
   Date dateofBirth; //YYYY-MM-DD  1900-2099

    public Person(String name, int age, String postalCode, double weightKg, Date dateofBirth) {
        this.name = name;
        this.age = age;
        this.postalCode = postalCode;
        this.weightKg = weightKg;
        this.dateofBirth = dateofBirth;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public Date getDateofBirth() {
        return dateofBirth;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setWeightKg(double weightKg) {
        this.weightKg = weightKg;
    }

    public void setDateofBirth(Date dateofBirth) {
        this.dateofBirth = dateofBirth;
    }

    

    public final void setName(String name) {
        if (name==null||name.length()<2){
            throw new IllegalArgumentException("name too short");
        }

        this.name = name;
    }

    public final void setAge(int age) {
        if ( age<0 || age>150){
             throw new IllegalArgumentException("age is invalid"); 
        }
        
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + '}';
    }

   
    
}
