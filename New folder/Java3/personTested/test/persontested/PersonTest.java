/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persontested;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ipd
 */
public class PersonTest {
    
    public PersonTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setName method, of class Person.
     */
    @Test
    public void testSetName() {
        System.out.println("setName where name is too short");
       String name = "";
        Person instance = new Person("Jerry", 40);
        
           boolean exception=false;
         try {
               instance.setName(null);
          }
          catch (IllegalArgumentException e) { 
              exception=true;
          }
          assertEquals("Exception expected for name "+ name, exception, true);
        
    }

    @Test
    public void testSetNamewithOne() {
        System.out.println("setName where name length is one");
        String name = "a";
        Person instance = new Person("Jerry", 40);
    
           boolean exception=false;
         try {
               instance.setName(name);
          }
          catch (IllegalArgumentException e) { 
              exception=true;
              fail("with one a");
          }
          assertEquals("Exception expected for name "+ name, exception, true);
        
    }
    
    
    @Test
    public void testSetNamewithtwo() {
        System.out.println("setName where name  length is two");
       String name = "aa";
        Person instance = new Person("Jerry", 40);
        
           boolean exception=false;
         try {
               instance.setName(name);
          }
          catch (IllegalArgumentException e) { 
              exception=true;
          }
          assertEquals("Exception expected for name "+ name, exception, true);
        
    }
    
        
    @Test
    public void testSetNamewithTwoorMore() {
        System.out.println("setName where name  length is more than two");
       String name = "aaasdf";
        Person instance = new Person("Jerry", 40);
        
           boolean exception=false;
         try {
               instance.setName(name);
          }
          catch (IllegalArgumentException e) { 
              exception=true;
               fail("with one two or more");
          }
          assertEquals("Exception expected for name"+ name, exception, true);
        
    }
    /**
    /**
     * Test of setAge method, of class Person.
     */
    @Test
    public void testSetAge1to150() {
        System.out.println("setAge...");
       // int age = 0;
        Person instance =  new Person("Jerry",1);
        
        for (int age=0; age<=150; age++) {
        instance.setAge(age);
        }
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        System.out.println("setAge 1 to 150 passed");
    }

    @Test
    public void testSetAgeOutofRangeAbove150(){
        
        System.out.println("SetAge out of range above 150....");
        Person instance = new Person("Jerry",1);
      for (int age=151; age < 250; age++) {
          boolean exception=false;
          try {
               instance.setAge(age);
          }
          catch (IllegalArgumentException e) { 
              exception=true;
          }
          assertEquals("Exception expected for age"+ age, exception, true);
        }
    }
    
    /**
     * Test of getName method, of class Person.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Person instance = null;
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAge method, of class Person.
     */
    @Test
    public void testGetAge() {
        System.out.println("getAge");
        Person instance = null;
        int expResult = 0;
        int result = instance.getAge();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Person.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Person instance = null;
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
 
    
     @Test
    public void testSetAgeOutofRangeNegative(){
        
        System.out.println("Setage ....");
        Person instance = new Person("Jerry",1);
      for (int age=-1; age> -100; age--) {
          boolean exception=false;
          try {
               instance.setAge(age);
          }
          catch (IllegalArgumentException e) { 
              exception=true;
          }
          assertEquals("Exception expected for age"+ age, exception, true);
        }
    }

    /**
     * Test of setAge method, of class Person.
     */
    @Test
    public void testSetAge() {
        System.out.println("setAge");
        int age = 0;
        Person instance = null;
        instance.setAge(age);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
