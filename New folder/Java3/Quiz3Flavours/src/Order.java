/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipd
 */
class Order {
    public int id;
    public String customerName;
    public String flavorList;        

    public Order(int id, String Customername, String flavorList) {
        this.id = id;
        this.customerName = customerName;
        this.flavorList = flavorList;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", customerName=" + customerName + ", flavorList=" + flavorList + '}';
    }
    
}
