/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todlitedb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Student
 */
public class Database {

    private Connection conn;

    public Database() throws SQLException {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:sqlite:c:/sqlite/test.sqlite");
                    
                    // "jdbc:mysql://localhost/task", "root", "root"
        } catch (SQLException e) {
            //    e.printStackTrace();
        }
    }

    public void addTask(String title, String dueDate, int isDoneInt) throws SQLException {
        String query = "Insert into task values (null,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, title);
        ps.setString(3, dueDate);
        ps.setInt(2, isDoneInt);
        ps.execute();
    }

    public ArrayList<TodoItem> getAllTasks() throws SQLException {
        final String SELECT_Item = "SELECT * from task";
        ArrayList<TodoItem> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_Item);
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                String dueDate = rs.getString("dueDate");
                boolean isDone = false;
                isDone = (rs.getInt("isDone") == 1);
                TodoItem t = new TodoItem(id, title, dueDate, isDone);
                System.out.println(t);
                result.add(t);
            }
        }
        return result;
    }

    public void updatePerson(int id, String name, int age) throws SQLException {
        String query = "update person.person set name = ? , age = ? where idperson = ?";
        PreparedStatement ps = conn.prepareStatement(query);
        name = "opoi";
        ps.setString(1, name);
        //age=100;
        ps.setInt(2, age);
        ps.setInt(3, id);
        ps.execute();

//    final String UPDATE_PERSON = "update person;";
        //   ps.execute();
        /*       String query = "update person; set name=? , age=? where idperson=?";
      //  ps = conn.prepareStatement(query);
      PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, name);
        ps.setInt(2, age);
        ps.setInt(3, id);
        ps.execute();
         */
    }

    public void deleteTask(int id) throws SQLException {
        final String DELETE_Task = "delete from task where id=" + id;
        PreparedStatement ps = conn.prepareStatement(DELETE_Task);
        ps.execute();
    }
}
