/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todlitedb;

/**
 *
 * @author Student
 */
class TodoItem {
    int id;
    String title;
    String dueDate;
    boolean isDone;

    public TodoItem(int id, String title, String dueDate, boolean isDone) {
        this.id = id;
        this.title = title;
        this.dueDate = dueDate;
        this.isDone = isDone;
    }

    @Override
    public String toString() {
       String status= "unfinished";
       if (isDone) {
           status= "finished";
       }
        return  title + " is due by " + dueDate + ", is " + status ;
    }
}
