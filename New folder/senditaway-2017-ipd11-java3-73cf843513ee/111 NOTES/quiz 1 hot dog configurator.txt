Quiz 1

Create project Quiz1HotDogSelector

Create GUI following instructions on the whiteboard.

File->Load Meat Types ...
File->Save Orders as ...
File->Exit

"Place order" button must be grayed out (disabled) until meat types are loaded.

File->Exit and window closing event check if any unsaved orders have been placed. 
If yes, then program must ask user to save orders in file with extension *.orders using save-as dialog.
If the user did not provide .orders extension then your program must add it.

Load meat types will read a *.meats file containing list of 4 meat types one per line and display it as text of 4 flavour buttons.
Assume the file has the 4 required lines, you do not have to verify that fact.
You must handle IOExceptions that may occur and communicate them to the user using a dialog.

Example file content of *.meats file:

German saussage
Vienna saussage
Meat balls
Vegetarian saussage


IMPORTANT: Each of the lines read from file becomes text of one of the 4 order buttons.

File->save orders as will save one order per line, same as displayed in orders placed text area box.

Your submission:
ZIP file of the entire project
