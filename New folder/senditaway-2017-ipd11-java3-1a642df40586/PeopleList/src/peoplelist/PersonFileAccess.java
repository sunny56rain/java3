package peoplelist;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class PersonFileAccess {

    final static String FILE_NAME = "people.txt";
    
    public static void savePersonListToFile(ArrayList<Person> list) throws IOException {        
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME, false)))) {
            for (Person p : list) {
                String line = String.format("%s;%d;%s", p.getName(), p.getAge(), p.getPostalCode());
                out.println(line);
            }
        }
    }

    public static ArrayList<Person> loadPersonListFromFile() throws IOException {
        throw new RuntimeException("Unimplemented");
    }

}
