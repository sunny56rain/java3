
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipd11
 */
public class FileAccess {
   final static String FILE_NAME = "save.txt";
    
    public static void saveListToFile(ArrayList<Trip> list) throws IOException {        
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME, false)))) {
            for (Trip t : list) {
                String line = String.format("%s;%s;%s;%tF;%tF %n", t.getDest(),t.getName(),t.getPassport(),t.getDepa(),t.getReturn());
                out.println(line);
            }
        }
    }
}
