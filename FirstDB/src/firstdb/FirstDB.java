package firstdb;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class FirstDB {

    public static final String DBUSER = "firstdb";
    public static final String DBPASS = "lNLBbprUIcmgfopm";

    public static void main(String[] args) throws SQLException {
        Scanner input = new Scanner(System.in);

        // lNLBbprUIcmgfopm
        //connect to database
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/firstdb", DBUSER, DBPASS);
        {
            //insert new data
            System.out.println("Enter name of city: ");
            String city = input.nextLine();
            System.out.println("Enter temperature: ");
            String temp = input.nextLine();
            System.out.println("Enter date in YYYY-MM-DD format: ");
            String date = input.nextLine();

            // do the insert
            String sql = "INSERT INTO weather (city, temperature, readingDate) VALUES (?, ?, ?)";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, city);
                stmt.setString(2, temp);
                stmt.setString(3, date);
                stmt.executeUpdate();
            }
        }

        // Task: execute SELECT of all records and display the date one record per line
        {
            String sql = "SELECT * FROM weather";
            try (Statement stmt = conn.createStatement(); ResultSet result = stmt.executeQuery(sql)) {

                while (result.next()) {
                    //Retrieve by column name
                    int id = result.getInt("ID");
                    String city = result.getString("city");
                    String temperature = result.getString("temperature");
                    Date readingDate = result.getDate("readingDate");
                    System.out.printf("%d: %s had %sC on %s\n", id, city, temperature, readingDate);
                }
            }
        }
    }
}
