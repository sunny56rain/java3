
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ipd11
 */
public class Database {

    public static final String DBUSER = "airports";
    public static final String DBPASS = "GHEopSUXirOnyqAV";

    private Connection conn;

    ArrayList<Airport> getAllAirport() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public Database() throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/airports", DBUSER, DBPASS);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
    }

    public void addAirport(Airport a) throws SQLException {
        String sql = "INSERT INTO airports (city, code, elevation, isInternational, latitude, longitude) VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, a.city);
            stmt.setString(2, a.code);
            stmt.setInt(3, a.elevation);
            stmt.setString(4, a.isInternational ? "domestic" : "international");
            stmt.setBigDecimal(5, a.latitude);
            stmt.setBigDecimal(6, a.longitude);
            stmt.executeUpdate();
        }
    }

    public void deleteAirport(int id) throws SQLException {
        String sql = "DELETE FROM airports WHERE ID=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            stmt.executeUpdate();
        }
    }

    public ArrayList<Airport> getAllAirport() throws SQLException {
        ArrayList<Airport> list = new ArrayList<>();
        String sql = "SELECT * FROM airports";
        try (Statement stmt = conn.createStatement(); ResultSet result = stmt.executeQuery(sql)) {
            while (result.next()) {
                Airport a = new Airport();
                a.id = result.getInt("ID");
                a.city = result.getString("city");
                a.code = result.getString("code");
                a.isInternational = result.getBoolean("isInternational");
                a.latitude = result.getBigDecimal("latitude");
                a.longitude = result.getBigDecimal("longitude");
                list.add(a);
            }
        }
        return list;
    }
}
