
import java.math.BigDecimal;
import java.util.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ipd11
 */
public class Quiz3Airport extends javax.swing.JFrame {

    Database db;

    DefaultListModel<Airport> airportListModel = new DefaultListModel<>();

    public Quiz3Airport() {
        try {
            db = new Database();
            initComponents();
            reloadAirport();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Fatal error opening database connection",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    private void reloadAirport() {
        try {
            ArrayList<Airport> airportList = db.getAllAirport();
            airportListModel.clear();
            for (Airport a : airportList) {
                airportListModel.addElement(a);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database access error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgAddEdit = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        dlgAddEdit_tfCity = new javax.swing.JTextField();
        dlgAddEdit_tfCode = new javax.swing.JTextField();
        dlgAddEdit_tfElevation = new javax.swing.JTextField();
        dlgAddEdit_tfLatitude = new javax.swing.JTextField();
        dlgAddEdit_tfLongitude = new javax.swing.JTextField();
        dlgAddEdit_btSave = new javax.swing.JButton();
        dlgAddEdit_btCancel = new javax.swing.JButton();
        dlgAddEdit_lblId = new javax.swing.JLabel();
        dlgAddEdit_cbIsInternational = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstAirport = new javax.swing.JList<>();
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        jLabel1.setText("ID");

        jLabel2.setText("City:");

        jLabel3.setText("Code:");

        jLabel4.setText("Elevation:");

        jLabel5.setText("Latitude:");

        jLabel6.setText("Longitude:");

        dlgAddEdit_btSave.setText("Save");
        dlgAddEdit_btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btSaveActionPerformed(evt);
            }
        });

        dlgAddEdit_btCancel.setText("Cancel");
        dlgAddEdit_btCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btCancelActionPerformed(evt);
            }
        });

        dlgAddEdit_lblId.setText("-");

        dlgAddEdit_cbIsInternational.setText("IsInternational");

        javax.swing.GroupLayout dlgAddEditLayout = new javax.swing.GroupLayout(dlgAddEdit.getContentPane());
        dlgAddEdit.getContentPane().setLayout(dlgAddEditLayout);
        dlgAddEditLayout.setHorizontalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dlgAddEdit_cbIsInternational)
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addComponent(dlgAddEdit_btSave)
                        .addGap(94, 94, 94)
                        .addComponent(dlgAddEdit_btCancel))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(46, 46, 46)
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dlgAddEdit_lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dlgAddEdit_tfCity, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_tfCode)
                            .addComponent(dlgAddEdit_tfElevation)))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(45, 45, 45)
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dlgAddEdit_tfLatitude, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_tfLongitude))))
                .addContainerGap(62, Short.MAX_VALUE))
        );
        dlgAddEditLayout.setVerticalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(dlgAddEdit_lblId))
                .addGap(24, 24, 24)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(dlgAddEdit_tfCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dlgAddEdit_tfCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(dlgAddEdit_tfElevation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(dlgAddEdit_cbIsInternational)
                .addGap(16, 16, 16)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(dlgAddEdit_tfLatitude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(dlgAddEdit_tfLongitude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_btSave)
                    .addComponent(dlgAddEdit_btCancel))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lstAirport.setModel(airportListModel);
        jScrollPane1.setViewportView(lstAirport);

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update");

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDelete)
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(33, 33, 33))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(51, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addGap(59, 59, 59)
                        .addComponent(btnUpdate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDelete)
                        .addGap(67, 67, 67))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        Airport selected = lstAirport.getSelectedValue();
        if (selected == null) {
            return;
        }
        int decision = JOptionPane.showConfirmDialog(
                this,
                "Are you sure you want to delete the following record?\n" + selected.toString(),
                "Confirm deletion",
                JOptionPane.YES_NO_OPTION);
        if (decision == JOptionPane.YES_OPTION) {
            try {
                db.deleteAirport(selected.id);
                reloadAirport();
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "Database access error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        dlgAddEdit.pack();
        dlgAddEdit_lblId.setText("-");
        dlgAddEdit_tfCity.setText("");
        dlgAddEdit_tfCode.setText("");
        dlgAddEdit_tfElevation.setText("");
        dlgAddEdit_cbIsInternational.isSelected();
        dlgAddEdit_tfLatitude.setText("");
        dlgAddEdit_tfLongitude.setText("");
        dlgAddEdit.setVisible(true);
    }//GEN-LAST:event_btnAddActionPerformed

    private void dlgAddEdit_btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btSaveActionPerformed
        String city = dlgAddEdit_tfCity.getText();
        if (city.length() < 2 || city.length() > 50) {
            JOptionPane.showMessageDialog(this,
                    "City name must be 2 to 50 characters long",
                    "Input error, city name invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        String code = dlgAddEdit_tfCode.getText();
        if (code.length() < 2 || code.length() > 5 || code.matches "([^A-Z$])") {
               JOptionPane.showMessageDialog(this,
                    "Code name must be 2 to 5 characters long\n must be UPPER CASE",
                    "Input error, city name invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        int elev = (Integer) dlgAddEdit_tfElevation.getText();
        if (elev < -10000 || elev > 10000) {
            JOptionPane.showMessageDialog(this,
                    "Elevation must be in -10000 to 10000",
                    "Input error, Elevation invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        int latit = (Integer) dlgAddEdit_tfLatitude.getText();
        if (elev < -90 || elev > 90) {
            JOptionPane.showMessageDialog(this,
                    "Latitude must be in -90 to 90",
                    "Input error, Latitude invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        int longit = (Integer) dlgAddEdit_tfLongitude.getText();
        if (elev < -180 || elev > 180) {
            JOptionPane.showMessageDialog(this,
                    "Longitude must be in -180 to 180",
                    "Input error, Longitude invalid",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        Airport a = new Airport();
        a.city = city;
        a.code = code;
        a.elevation = new elevation(elev);
        a.isInternational = isInternational;
        a.latitude = new BigDecimal(latit);
        a.longitude = new BigDecimal(longit);

        try {
            db.addAirport(a);
            dlgAddEdit.setVisible(false);
            reloadAirport();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Database access error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgAddEdit_btSaveActionPerformed

    private void dlgAddEdit_btCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btCancelActionPerformed
        dlgAddEdit.setVisible(false);
    }//GEN-LAST:event_dlgAddEdit_btCancelActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Quiz3Airport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Quiz3Airport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Quiz3Airport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Quiz3Airport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Quiz3Airport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JDialog dlgAddEdit;
    private javax.swing.JButton dlgAddEdit_btCancel;
    private javax.swing.JButton dlgAddEdit_btSave;
    private javax.swing.JCheckBox dlgAddEdit_cbIsInternational;
    private javax.swing.JLabel dlgAddEdit_lblId;
    private javax.swing.JTextField dlgAddEdit_tfCity;
    private javax.swing.JTextField dlgAddEdit_tfCode;
    private javax.swing.JTextField dlgAddEdit_tfElevation;
    private javax.swing.JTextField dlgAddEdit_tfLatitude;
    private javax.swing.JTextField dlgAddEdit_tfLongitude;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<Airport> lstAirport;
    // End of variables declaration//GEN-END:variables
}
