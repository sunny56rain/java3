package whattododb;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Database {

    public static final String DBUSER = "whattodo";
    public static final String DBPASS = "R8B3hfTTUHzQA4Tm";

    private Connection conn;

    public Database() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/whattodo", DBUSER, DBPASS);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
    }

    public void addTodo(Todo item) throws SQLException {
        String sql = "INSERT INTO todos (task, dueDate, isDone) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, item.task);
            stmt.setDate(2, new java.sql.Date(item.dueDate.getTime()));
            stmt.setString(3, item.isDone ? "true" : "false");
            stmt.executeUpdate();
        }
    }

    public void updateTodo(Todo w) {
        throw new RuntimeException("Update record not implemented yet");
    }

    public void deleteTodo(long id) throws SQLException {
        String sql = "DELETE FROM weather WHERE ID=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        }
    }

    public ArrayList<Todo> getAllTodo() throws SQLException {
        ArrayList<Todo> list = new ArrayList<>();
        String sql = "SELECT * FROM weather";
        try (Statement stmt = conn.createStatement(); ResultSet result = stmt.executeQuery(sql)) {
            while (result.next()) {
                Todo w = new Todo();
                w.id = result.getLong("ID");
                w.city = result.getString("city");
                w.temperature = result.getBigDecimal("temperature");
                w.readingDate = result.getDate("readingDate");
                list.add(w);
            }
        }
        return list;
    }

    public Todo getTodoById(long id) {
        throw new RuntimeException("Fetch one record not implemented yet");
    }

}

