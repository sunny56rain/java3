
package whattododb;

import java.util.Date;

public class Todo {
	long id; // PK INT AI
	String task; // VARCHAR(50)
	Date dueDate; // DATE
	boolean isDone; // INTEGER in database 0 is false, 1 is true
}
