package chooseflavour;

import java.util.Scanner;

public class ChooseFlavour {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Choose an option 1-4:\n"
                + "1. Vanilla\n"
                + "2. Strawberry\n"
                + "3. Chocolate\n"
                + "4. Surprise me");
        int choice = input.nextInt();

        switch (choice) {
            case 1: System.out.println("You get the best Vanilla in EST time zone");
                break;
            case 2: System.out.println("Strawberries hand picked");
                break;
            case 3: System.out.println("Chocolate brown");
                break;
            case 4: System.out.println("Random?");
                break;
            default:
                System.out.println("Invalid choice");
        }
        
    }

}
