package agequestion;

import java.util.Scanner;

public class AgeQuestion {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("What is your age?");
        int age = input.nextInt();
        
        if (age < 18) {
            System.out.println("you're underage");
        } else if (age < 65) {
            System.out.println("you're working age");
        } else {
            System.out.println("you're retired");
        }
    }
    
}
