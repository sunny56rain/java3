package calcmenu;

import java.util.Scanner;

public class CalcMenu {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please make a choice 1-4:\n"
                + "1. Add two floating point numbers\n"
                + "2. Enter 3 integer numbers and find largest one\n"
                + "3. Check if integer number divides by both 3 and 7\n"
                + "4. Enter radius and get surface of sphere");
        int choice = input.nextInt();
        switch (choice) {
            case 1: {
                System.out.println("Please enter 2 numbers");
                double v1 = input.nextDouble();
                double v2 = input.nextDouble();
                System.out.printf("%f + %f = %.5f", v1, v2, v1+v2);
                //
            }
            break;
            case 2: {
                System.out.println("Enter 3 numbers");
                int v1 = input.nextInt();
                int v2 = input.nextInt();
                int v3 = input.nextInt();
                ///
                if (v1 > v2) {
                    if (v1 > v3) {
                        System.out.println("largest " + v1);
                    } else {
                        System.out.println("largest " + v3);
                    }
                } else {
                    if (v2 > v3) {
                        System.out.println("largest " + v2);
                    } else {
                        System.out.println("largest " + v3);
                    }
                }
            }
            break;
            case 3: {
                System.out.println("Enter an integer");
                int v1 = input.nextInt();
                boolean isDivisble = v1 % 3 == 0 && v1 % 7 == 0;
                System.out.println("Result: " + isDivisble);
                if (isDivisble) {
                    System.out.printf("%d is divisible both by 7 and 3", v1);
                } else {
                    System.out.printf("No, %d is not divisible by both 7 and 3", v1);
                }                
            }
            break;
            case 4:
                break;
            default:
                System.out.println("Invalid choice");
        }

    }

}
