package whoareyou;

import java.util.Scanner;

public class WhoAreYou {

    public static void main(String[] args) {

        char c = '\'';
        char r = '\n';

        //byte bbbb = 1000;
        final double PI_NUMBER = 3.141592;

        long value = 13242323442434L;

        int v2 = 23;
        long result = 45326343453543553L / v2;

        double something = 234.234;
        float aaaaa = 234.33F;

        String name;
        int age;

        Scanner input = new Scanner(System.in);

        // ask user for information
        System.out.println("What is your name?");
        name = input.nextLine();
        System.out.println("How old are you?");
        age = input.nextInt();
        System.out.println("How tall are you in meters?");
        double heightMeters = input.nextDouble();
        int heightInches = (int) (heightMeters / 0.0254);

        System.out.println("Nice to meet you " + name + "!");
        System.out.println("You are " + age + " years old.");
        System.out.println("You are " + heightInches + " tall");

        System.out.println(name + "\"s \t\theight \nis " + heightInches
                + "\" \\ " + heightMeters + "m");

    }

}
